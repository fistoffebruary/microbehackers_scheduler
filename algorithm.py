
'''
output_schedule takes one pandas dataframe as input; this dataframe
must have 7 columns representing the 7 days of the week; it must have
29 rows representing every time between 7:00 AM and 9:00 PM in 30 minute increments

self.df will be incremented by 1 at times where it is assigned; self.name_df will
have the names associated with the assigned schedules appended to it where schedules
are assigned.
'''


class output_schedule():
    def __init__(self, df):
        self.df = df
        self.name_df = df.copy()

    '''
    get_schedule returns a pandas dataframe of the output
    schedule; each value will be an integer
    '''

    def get_schedule(self):
        return self.df

    '''
    get_name_schedule returns a pandas dataframe of the output
    schedule; each value will be a string containing the names
    of people assigned to the output schedule
    '''

    def get_name_schedule(self):
        return self.name_df

    '''
    the assign method increments self.df by 1 where a schedule has been assigned
    to the output schedule
    '''

    def assign(self, indices):
        self.df.iloc[indices[1]:indices[2] + 1, indices[0]] += 1

    '''
    name_assign adds the name of a schedule object where said schedule
    has been assigned to the output schedule
    '''

    def name_assign(self, schedule, indices):
        for i in range(indices[1], indices[2] + 1):
            self.name_df.iloc[i, indices[0]] = "{0}---{1}".format(
                str(self.name_df.iloc[i, indices[0]]), schedule.get_name())


'''
schedule takes pandas dataframe df, a string name, and an optional
parameter max_scheduled hours that defaults to integer 8. self.df
must have 7 columns representing the 7 days of the week; it must have
29 rows representing every time between 7:00 AM and 9:00 PM in 30 minute increments

schedule represents the object assigned to each person being assigned. A schedule
object have a business associated with them which amounts to how many occupied time slots
there are in their schedule. Business can be retrieved for the
whole week, the weekdays, or a particular day. A schedule has scheduled hours which
equate to the number of hours a schedule has been assigned to the lab.
Schedules can interact with time blocks to determine if they are free at the time block.

Schedules can be assigned to time blocks by extracting the time blocks' indices; assigning
a time block will also increase the assigned hours by the size of the time block.
Availability assigning a schedule to indices will assign the schedule without increasing its
assigned hours
'''


class schedule():
    def __init__(self, df, name, max_scheduled_hours=8):
        self.df = df
        self.max_scheduled_hours = max_scheduled_hours
        self.scheduled_hours = 0
        self.name = name
        self.time_blocks_assigned = 0

    '''
    get_schedule returns a pandas dataframe of the schedule
    '''

    def get_schedule(self):
        return self.df

    '''
    get_total_business returns the sum of all 1s on the schedule
    effectively returning how "busy" a schedule is
    '''

    def get_total_business(self):
        return self.df.sum().sum()

    '''
    get_total_business_day accepts a day as a string (all lowercase)
    and returns the integer sum of all 1s of a given day on the
    schedule
    '''

    def get_total_business_day(self, day):
        return self.df.sum()[day]

    '''
    get_total_business_weekday returns the integer sum of all 1s during the weekdays
    of a given schedule
    '''

    def get_total_business_weekday(self):
        df = self.df.copy()
        df = df.drop("sunday", axis=1)
        df = df.drop("saturday", axis=1)
        return df.sum().sum()

    '''
    get_scheduled_hours returns the integer number of hours that have
    been added to the schedule
    '''

    def get_scheduled_hours(self):
        return self.scheduled_hours

    '''
    get_max_scheduled_hours returns the maximum integer number of hours a
    schedule can be assigned
    '''

    def get_max_scheduled_hours(self):
        return self.max_scheduled_hours

    '''
    set_scheduled_hours increments the scheduled hours by
    integer or floating point num_hours_to_add
    '''

    def set_scheduled_hours(self, num_hours_to_add):
        self.scheduled_hours += num_hours_to_add

    '''
    at_max_scheduled_hours returns True if the schedule's
    scheduled_hours equal its max scheduled hours and False
    otherwise
    '''

    def at_max_scheduled_hours(self):
        return self.scheduled_hours == self.max_scheduled_hours

    '''
    above_max_scheduled_hours returns True if the schedule's
    scheduled_hours are greater than its max scheduled hours
    and False otherwise
    '''

    def above_max_scheduled_hours(self):
        return self.scheduled_hours > self.max_scheduled_hours

    '''
    schedule_free_at_TB returns True if the schedule is
    is completely free for the entirety of a time block
    and False otherwise
    '''

    def schedule_free_at_TB(self, TB):
        indices = TB.get_indices()
        day = self.df.columns[indices[0]]
        for i in self.df[day][indices[1]:indices[2] + 1]:
            if i == 1:
                return False
        return True

    '''
    schedule_free_at_indices returns True if the schedule is
    is completely free for the entirety of a set of indices
    and False otherwise
    '''

    def schedule_free_at_indices(self, indices):
        day = self.df.columns[indices[0]]
        for i in self.df[day][indices[1]:indices[2] + 1]:
            if i == 1:
                return False
        return True

    '''
    get_available_number_TB returns the integer number of time blocks
    a given schedule could potentially be scheduled to
    '''

    def get_available_number_TB(self, time_blocks):
        time_blocks = [
            i for i in time_blocks if not i.at_max_number_of_people()]
        return len([item for item in time_blocks if self.schedule_free_at_TB(item)])

    '''
    get_available_TB returns the list of time blocks
    a given schedule could potentially be scheduled to
    '''

    def get_available_TB(self, time_blocks):
        time_blocks = [
            i for i in time_blocks if not i.at_max_number_of_people()]
        # get a list of all the time blocks for which schedule_free_at_TB() returns true
        return [item for item in time_blocks if self.schedule_free_at_TB(item)]

    '''
    is_assignable takes a list of time blocks as input and returns true
    if the schedule could be potentially assigned to 1
    or more time blocks
    '''

    def is_assignable(self, time_blocks):
        return self.get_available_number_TB(time_blocks) > 0

    '''
    assign takes an indexable iterable of indices [day, start, end]
    and increments the self.df by 1 at the indices; will add 0.5 hours
    to scheduled_hours for each time slot added
    '''

    def assign(self, indices):
        # indices always in the [day, start, end] format
        self.df.iloc[indices[1]:indices[2] + 1, indices[0]] = 1
        time_added = ((indices[2] - indices[1]) * 0.5) + 0.5
        self.scheduled_hours += time_added

    '''
    assign_availability takes an indexable iterable of indices [day, start, end]
    and increments the self.df by 1 at the indices
    '''

    def assign_availability(self, indices):
        self.df.iloc[indices[1]:indices[2] + 1, indices[0]] = 1

    '''
    get_name returns the name of the schedule
    '''

    def get_name(self):
        return self.name

    '''
    get_number_time_blocks_assigned returns integer number
    of time blocks to which a person has been assigned
    '''

    def get_number_time_blocks_assigned(self):
        return self.time_blocks_assigned

    '''
    add_time_block_assigned increments the integer number of
    time_blocks_assigned by the integer number_of_new_time_blocks
    '''

    def add_time_block_assigned(self, number_of_new_time_blocks):
        self.time_blocks_assigned += number_of_new_time_blocks


'''
schedule_assigner takes a blank output_schedule (pandas df), a list
of schedule objects, and a list of time block objects as input

schedule_assigner is essentially the master class which manages all
other classes

schedule_assigner takes several approaches to scheduling and can use
all available methods to avoid making invalid schedules

schedule_assigner has several status methods to track the each person
and each time block to ensure everyone is assigned fully and effectively
'''


class schedule_assigner():
    def __init__(self, output_schedule, schedules, time_blocks):
        self.output_schedule = output_schedule
        self.schedules = schedules
        self.time_blocks = time_blocks

    '''
    checks takes i, a schedule, and j, a time block as input;
    checks makes sure that i is free at j and that adding j's hours to i does not
    overassign i and that adding i to j does not assign too many people to j.
    '''

    def checks(self, i, j):
        return i.schedule_free_at_TB(j) and i.get_scheduled_hours() + j.get_hours_long() <= i.get_max_scheduled_hours() and not j.at_max_number_of_people()

    '''
    checks_indices takes i, a schedule, and j, an indexable iterable
    of indices [day, start, end] as input; checks makes sure that i is
    available at indices.
    '''

    def checks_indices(self, i, indices):
        return i.schedule_free_at_indices(indices)

    '''
    sort schedules in the schedules list such that the busiest schedule
    is at the first index
    '''

    def sort_schedules_descending_business(self, lst):
        pass
        # create a list of tuples; the first item in each tuple is the schedule object, the second is the weekday business
        lst = [(i, i.get_total_business_weekday()) for i in lst]
        # extract the schedules only (sorted by descending business)
        return [i[0] for i in sorted(lst, key=lambda x: x[1], reverse=True)]

    '''
    provides a schedule of availability, and, depending on if indices_only
    is true or false. If true, will provide an availability schedule by hour.
    If false, will provide an availability schedule by lab sections
    '''

    def assign_availability_schedule(self, indices_only=False):
        if indices_only:
            indices = [(i, j, j) for i in range(0, 7) for j in range(0, 29)]
            for i in self.schedules:
                for j in indices:
                    if self.checks_indices(i, j):
                        i.assign_availability(j)
                        self.output_schedule.assign(j)
                        self.output_schedule.name_assign(i, j)
        else:
            for i in self.schedules:
                for j in self.time_blocks:
                    indices = j.get_indices()
                    if self.checks_indices(i, indices):
                        i.assign_availability(indices)
                        self.output_schedule.assign(indices)
                        self.output_schedule.name_assign(i, indices)
    '''
    Sorts the list of schedules by busiest hours to least busiest.
    Then proceeds to check if the schedule is available for each
    time block. If it is, then it will assign the person to the time
    block.
    '''

    def assign_for(self):
        self.schedules = self.sort_schedules_descending_business(
            self.schedules)
        for i in self.schedules:
            for j in self.time_blocks:
                if self.checks(i, j):
                    i.assign(j.get_indices())
                    j.add_person(i)
                    self.output_schedule.assign(j.get_indices())
                    self.output_schedule.name_assign(i, j.get_indices())
    '''
    similar to assign_for method except it takes into account the
    priority of each time block, and keeps removing people
    from the schedule list if their hours are at a certain
    number (in this case 8). It will continue until cnt is
    over 100, which occurs if the while loop cannot continue
    adding people to the output.
    '''

    def assign_while(self):
        from random import randint
        self.schedules = self.sort_schedules_descending_business(
            self.schedules)
        cnt = 0
        schedules = self.get_people_not_on_threshold(self.schedules, 8)
        schedules = [i for i in schedules if i.is_assignable(self.time_blocks)]
        time_blocks = [
            i for i in self.time_blocks if not i.at_max_number_of_people()]
        for i in time_blocks:
            i.set_priority_from_hours()
        while cnt < 100 and len(schedules) >= 1:
            try:
                if len(time_blocks) < 6:
                    for i in self.time_blocks:
                        i.increment_max_number_of_people()
                    time_blocks = self.time_blocks
                    time_blocks = [
                        i for i in self.time_blocks if not i.at_max_number_of_people()]
                    for i in time_blocks:
                        i.set_priority_from_hours()

                schedules = self.get_people_not_on_threshold(schedules, 8)
                schedules = [
                    i for i in schedules if i.is_assignable(time_blocks)]
                time_blocks = [
                    i for i in time_blocks if not i.at_max_number_of_people()]
                time_blocks = [j[0] for j in sorted(
                    [(i, i.get_priority()) for i in time_blocks], key=lambda x: x[1], reverse=True)]
                sch = schedules[(cnt % len(schedules)) - 1]
                TB = time_blocks[0]
                if self.checks(sch, TB):
                    sch.assign(TB.get_indices())
                    sch.add_time_block_assigned(1)
                    TB.add_person(sch)
                    self.output_schedule.assign(TB.get_indices())
                    self.output_schedule.name_assign(sch, TB.get_indices())
                    cnt -= 1
                cnt += 1
                TB.change_priority_by_number(-1)

            except ZeroDivisionError:  # pragma: no cover
                break

                # need to block 4 from 3

    # def time_slot(self, time_slot_day, time_slot_start, time_slot_end):
        #specific_TBs = []
        #tbs_in_day = [i for i in self.time_blocks if self.day == time_slot_day]
        # idk how to make this into actual code :(
        # for i in tbs_in_day:
            # if_time_slot_start == "XX:30:00":
                #time_slot_start == "XX+1:00:00"
            # if time_slot_end == "XX:30:00":
                #time_slot_end == "XX+1:00:00"

            # if time_slot_start is in this time block:
                # if time_slot_end is in same time block:
                # specific_TBs.append(new_time_block(...))
                # elif time_slot_end is between another time block:
                # specific_TBs.append(new_time_block(...))

        # return specific_TBs

        # identify day
        # has to be 00:00 increment
        # identify the time block that it starts AND ends
        # identifies if the 2 time_blocks that are encompassed are available
        # if true:
        # returns new set of time_blocks that assign_outside_TB will take in
        # and place them in those specified blocks
        # assign_outside_TB might rerun this if a person gets put in and
        # renders previous TB null
        # that or we do it one at a time.

    # identify which time block this interval encompasses
    # def assign_outside_TB(self):
        # AHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH
        #schedules = self.get_people_not_on_threshold(schedules, 8)

        #time_blocks = [i for i in specific_TBs]
        # while cnt < 100:
            #

        # doesnt put people w max hours in list
        # doesnt take into account time blocks that are already filled
        # if between 2 different TB, will need to take into account both
        # number of people in both TBs
        # will do 4 hours blocks only at this point
        # unless it goes over their hours

    '''
    returns the output schedule. Data Type = object
    '''

    def get_output_schedule_object(self):
        return self.output_schedule
    '''
    returns a list of schedules that are not over a certain
    threshold in lab hours assigned
    '''

    def get_people_not_on_threshold(self, schedules, threshold):
        return [i for i in schedules if i.get_scheduled_hours() < threshold]
    '''
    returns a list of names and hours the name is scheduled
    from a list of schedules. If verbose=false, then it will
    return a list of names and hours for people who still
    have hours they need assigned.
    '''

    def status_people_under_assigned(self, verbose=False):
        if verbose:
            return [(i.get_name(), i.get_scheduled_hours()) for i in self.schedules if not i.at_max_scheduled_hours()]
        return [i.get_name() for i in self.schedules if not i.at_max_scheduled_hours()]

    '''
    will return an integer number of people who are not at their
    max capacity hours
    '''

    def status_number_people_under_assigned(self):
        return len([i.get_name() for i in self.schedules if not i.at_max_scheduled_hours()])
    '''
    will return a list of people and the number of time blocks
    they are assigned to from a list of schedules
    '''

    def status_people_num_time_blocks(self):
        return [(i.get_name(), i.get_number_time_blocks_assigned()) for i in self.schedules]
    '''
    will return a list of names of people who were not assigned a
    lab section.
    '''

    def status_people_with_0_time_blocks(self):
        return [i.get_name() for i in self.schedules if i.get_number_time_blocks_assigned() == 0]


'''
time_block represents each lab section as an object

as input, it takes the day as a string,
the section (AM/PM),
the hours_long as a float,
start time as a string (corresponding to the row name in schedule dataframes),
end time as a string (corresponding to the row name in schedule dataframes),
and empty input pandas dataframe (formatted as a schedule),
an optional integer max_number of people that defaults to 4,
and an optional priority that defaults to integer 0

time_block has methods to manage, reset, and retrieve its attributes

time_blocks can be assigned to schedules and can have schedules assigned to them
'''


class time_block():
    def __init__(self, day, section, hours_long, start_time, end_time, df, max_number_of_people=4, priority=0):
        self.day = day
        self.section = section
        self.hours_long = hours_long
        self.max_number_of_people = max_number_of_people
        self.people_assigned = []
        self.number_of_people = len(self.people_assigned)
        self.start_time = start_time
        self.end_time = end_time
        self.df = df
        self.priority = priority

    '''
    will return a string of what day a time_block is
    '''

    def get_day(self):
        return self.day
    '''
    will return a string of what section a time_block is
    '''

    def get_section(self):
        return self.section
    '''
    will return an integer of how long the time_block is
    '''

    def get_hours_long(self):
        return self.hours_long
    '''
    will return an integer of the maximum number of people
    that can be in a lab section
    '''

    def get_max_number_of_people(self):
        return self.max_number_of_people
    '''
    will increase the maximum number of people a time_block
    can hold by 1 if called
    '''

    def increment_max_number_of_people(self):
        self.max_number_of_people += 1
    '''
    reads in a list of schedules, and adds to the list of people
    assigned to the time_block and incrmeents the number of
    people assigned by 1
    '''

    def add_schedules(self, *schedules):
        for i in schedules:
            self.people_assigned.append(i)
            self.number_of_people += 1
    '''
    will return True/False if the time_block is at max capacity
    '''

    def at_max_number_of_people(self):
        return len(self.people_assigned) == self.max_number_of_people
    '''
    will return True/False depending on if the number of people
    assigned is over the max number
    '''

    def above_max_number_of_people(self):
        return self.number_of_people > self.max_number_of_people
    '''
    will return an integer number of people that are assigned
    to the time_block
    '''

    def get_number_of_people(self):
        return len(self.people_assigned)
    '''
    will return the list of people assigned to the time block
    '''

    def get_people_assigned(self):
        return self.people_assigned
    '''
    will return the day, start time, and end time indices
    of the time_block in relation to where it is on
    the csv file.
    '''

    def get_indices(self):
        day_index = 0
        start_index = 0
        end_index = 0

        while self.day != self.df.columns[day_index]:
            day_index += 1

        while self.start_time != self.df.index[start_index]:
            start_index += 1

        while self.end_time != self.df.index[end_index]:
            end_index += 1

        return [day_index, start_index, end_index - 1]
    '''
    will take in a number of people and add them to the list of
    people assigned to the time block
    '''

    def add_person(self, *people):
        for i in people:
            self.people_assigned.append(i)
    '''
    will return an integer of the priority of the time_block
    '''

    def get_priority(self):
        return self.priority
    '''
    if called, will change the priority of the time block by
    adding the number it takes in to the current priority
    number
    '''

    def change_priority_by_number(self, setting):
        self.priority += setting
    '''
    if called, will change the priority number to the number it
    takes in
    '''

    def set_priority(self, setting):
        self.priority = setting
    '''
    sets the priority number to 10 if the number of hours
    the time_block is equals 4
    '''

    def set_priority_from_hours(self):
        if self.hours_long == 4:
            self.priority = 10


'''
formatter takes a name_df (pandas dataframe) (27 x 7) (every 30 minutes)
and an output df (pandas dataframe) (13 x 7) (every hour);
it makes sure that 9:00:00 PM is removed from its input schedules

formatter can process a raw name_df and output it as an alphebetized
schedule displaying every hour. it places the number of people assigned to each
time slot/block at the beginning of the cell
'''


class formatter():
    def __init__(self, name_df, output_df_hour):
        self.name_df = name_df
        self.output_df_hour = output_df_hour
        try:
            self.output_df_hour = self.output_df_hour.drop("9:00:00 PM")
            self.name_df = self.name_df.drop("9:00:00 PM")
        except KeyError:
            pass

    '''
    get_name_df returns the name_df as a pandas dataframe
    '''

    def get_name_df(self):
        return self.name_df

    '''
    get_output_df_hour returns the output_df_hour (pandas dataframe);
    this is the end result schedule and represents the final
    output for the program
    '''

    def get_output_df_hour(self):
        return self.output_df_hour

    '''
    remove_0_ removes the `0---` character from each cell; applies
    to the "every 30 minutes schedules"
    '''

    def remove_0_(self):
        for i in range(0, 7):
            for j in range(0, 28):
                self.name_df.iloc[j, i] = str(
                    self.name_df.iloc[j, i]).replace("0---", "")

    '''
    prepend_number_of_people calculates the number of people
    assigned to a given time slot. It then prepends this number to
    each time slot and delimits this number and the list of names
    with `:::`
    '''

    def alphabetize(self):
        for i in range(0, 7):
            for j in range(0, 14):
                self.output_df_hour.iloc[j, i] = "---".join(
                    sorted(str(self.output_df_hour.iloc[j, i]).split("---")))

    '''
    prepend_number_of_people calculates the number of people
    assigned to a given time slot. It then prepends this number to
    each time slot and delimits this number and the list of names
    with `:::`
    '''

    def prepend_number_of_people(self):
        for i in range(0, 7):
            for j in range(0, 14):
                if str(self.output_df_hour.iloc[j, i]) == "0":
                    self.output_df_hour[j, i] = 0
                else:
                    x = len(
                        set(str(self.output_df_hour.iloc[j, i]).split("---")))
                    y = "{0}:::{1}".format(x, self.output_df_hour.iloc[j, i])
                    self.output_df_hour.iloc[j, i] = y

    '''
    format_to_free_for_full_hour collapses a 27 x 7 schedule with
    cells for 30 minute intervals to a 13 x 7 schedule with cells
    for 1 hour intervals
    '''

    def format_to_free_for_full_hour(self):
        for i in range(0, 7):
            for j in range(0, 28, 2):
                x = (set(str(self.name_df.iloc[j, i]).split("---")))
                y = (set(str(self.name_df.iloc[j + 1, i]).split("---")))
                intersect = "---".join(list(x.intersection(y)))
                self.output_df_hour.loc[self.name_df.index[j],
                                        self.name_df.columns[i]] = intersect

    '''
    formatting macro_1 runs all formatter methods; this results
    in the `0---` being removed from each cell, the output being reduced
    from every 30 minutes to every hour, each cell's names being alphabetized,
    and the number of each people in a time slot being prepended to the cell
    '''

    def formatting_macro_1(self):
        self.remove_0_()
        self.format_to_free_for_full_hour()
        self.alphabetize()
        self.prepend_number_of_people()
        cols = self.get_output_df_hour().columns[7:]
        self.output_df_hour = self.output_df_hour.drop(cols, axis=1)
