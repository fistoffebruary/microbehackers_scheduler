from unittest import main, TestCase

from csmo_parser import time_convert_helper, get_index_helper, parse_csmo_make_schedule

from algorithm import output_schedule, schedule, schedule_assigner, time_block, formatter

import pandas as pd

from time_blocks_importer import func

class TestScheduler(TestCase):

    # ----
    # time_convert_helper
    # ----

    def test_time_convert_helper_1(self):
        miltime = "23:00"
        stdtime = time_convert_helper(miltime)
        self.assertEqual(stdtime, "11:00:00 PM")

    def test_time_convert_helper_2(self):
        miltime = "1:00"
        stdtime = time_convert_helper(miltime)
        self.assertEqual(stdtime, "1:00:00 AM")

    def test_time_convert_helper_3(self):
        miltime = "12:00"
        stdtime = time_convert_helper(miltime)
        self.assertEqual(stdtime, "12:00:00 PM")

    def test_time_convert_helper_4(self):
        miltime = "12:30"
        stdtime = time_convert_helper(miltime)
        self.assertEqual(stdtime, "12:30:00 PM")

    # ----
    # get_index_helper
    # ----

    def test_get_index_helper_1(self):
        lst = ["abc", "def", "ghi"]
        key = "abc"
        idx = get_index_helper(lst, key)
        self.assertEqual(idx, 0)

    def test_get_index_helper_2(self):
        lst = ["abc", "def", "ghi"]
        key = "def"
        idx = get_index_helper(lst, key)
        self.assertEqual(idx, 1)

    def test_get_index_helper_3(self):
        lst = ["abc", "def", "ghi"]
        key = "ghi"
        idx = get_index_helper(lst, key)
        self.assertEqual(idx, 2)

    def test_get_index_helper_4(self):
        lst = ["abc", "def", "ghi"]
        key = "jkl"
        idx = get_index_helper(lst, key)
        self.assertEqual(idx, None)

    # ----
    # parse_csmo_make_schedule
    # ----

    def test_parse_csmo_make_schedule_1(self):
        parse_csmo_make_schedule(
            'Input/Input_Template.csv', "test_files/Schedule_Test_1.csmo", directory_path=None)
        df = pd.read_csv("test_files/Schedule_Test_1.csv", index_col=0)
        test = pd.read_csv(
            "test_files/Schedule_Test_1_Answer.csv", index_col=0)
        assert df.equals(test)

    def test_parse_csmo_make_schedule_2(self):
        parse_csmo_make_schedule(
            'Input/Input_Template.csv', "test_files/TestSchedule_2.csmo", directory_path=None)
        df = pd.read_csv("test_files/TestSchedule_2.csv", index_col=0)
        test = pd.read_csv(
            "test_files/Schedule_Test_2_Answer.csv", index_col=0)
        assert df.equals(test)

    def test_parse_csmo_make_schedule_3(self):
        parse_csmo_make_schedule(
            'Input/Input_Template.csv', directory_path="test_files")

    # ----
    # output_schedule class
    # ----
        # ----
        # get_schedule
        # ----
    def test_method__get_schedule_1(self):

        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = output_schedule(df)
        assert df.equals(x.get_schedule())

    def test_output_schedule_assign_1(self):
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        sched = schedule(df, "Ryan Bailey")
        TB = time_block("tuesday", "PM", 4, "3:00:00 PM", "7:00:00 PM", df, 3)
        output.assign(TB.get_indices())

    def test_name_assign_1(self):
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        sched = schedule(df, "Ryan Bailey")
        TB = time_block("tuesday", "PM", 4, "3:00:00 PM", "7:00:00 PM", df, 3)
        output.name_assign(sched, TB.get_indices())
    # ----
    # schedule class
    # ----
        # ----
        # get_schedule
        # ----

    def test_schedule_method__get_schedule_1(self):

        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        assert df.equals(x.get_schedule())

        # ----
        # get_max_scheduled_hours
        # ----

    def test_get_max_scheduled_hours_1(self):
        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_max_scheduled_hours(), 8)

        # ----
        # get_total_business
        # ----

    def test_schedule_method__get_total_business_1(self):

        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_total_business(), 0)

    def test_schedule_method__get_total_business_2(self):

        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_total_business(), 28)

        # ----
        # get_total_business_day
        # ----
    def test_schedule_method__get_total_business_day_1(self):

        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_total_business_day("sunday"), 8)

        # ----
        # get_total_business_weekday
        # ----
    def test_schedule_method__get_total_business_weekday_1(self):

        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_total_business_weekday(), 20)

        # ----
        # get_name
        # ----
    def test_schedule_method__get_name_1(self):

        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_name(), "Ryan Bailey")

        # ----
        # scheduled_hours
        # ----
    def test_get_scheduled_hours_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_scheduled_hours(), 0)

    def test_set_scheduled_hours_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        x.set_scheduled_hours(8)
        self.assertEqual(x.get_scheduled_hours(), 8)

    def test_abt_max_scheduled_hours_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        x.set_scheduled_hours(8)
        self.assertEqual(x.at_max_scheduled_hours(), True)

    def test_above_max_scheduled_hours_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        x.set_scheduled_hours(13)
        self.assertEqual(x.above_max_scheduled_hours(), True)

        # ----
        # check if schedule is free
        # ----

    def test_schedule_free_at_TB_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        TB = time_block("tuesday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        # self.assertEqual(x.schedule_free_at_TB(TB), False)
        self.assertEqual(x.schedule_free_at_TB(TB), False)

    def test_schedule_free_at_TB_2(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        TB = time_block("tuesday", "PM", 4, "3:00:00 PM", "7:00:00 PM", df, 3)
        # self.assertEqual(x.schedule_free_at_TB(TB), False)
        self.assertEqual(x.schedule_free_at_TB(TB), True)

    def test_get_available_number_TB(self):
        time_blocks = func()
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        self.assertEqual(x.get_available_number_TB(time_blocks), 9)

    def test_get_available_TB(self):
        time_blocks = func()
        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = schedule(df, "Output_Template")
        self.assertEqual(x.get_available_TB(time_blocks), time_blocks)

    def test_is_assignable(self):
        time_blocks = func()
        # True test
        df = pd.read_csv("Output/Output_Template.csv", index_col=0)
        x = schedule(df, "Output_Template")
        self.assertEqual(x.is_assignable(time_blocks), True)
        # False test
        df = pd.read_csv("test_files/Schedule_Test_3.csv", index_col=0)
        y = schedule(df, "Schedule_Test_3")
        self.assertEqual(y.is_assignable(time_blocks), False)

        # ----
        # assign
        # ----

    def test_assign_1(self):  # pragma: no cover
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = schedule(df, "Ryan Bailey")
        TB = time_block("tuesday", "PM", 4, "3:00:00 PM", "7:00:00 PM", df, 3)
        indices = TB.get_indices()
        assert x.schedule_free_at_TB(TB)
        x.assign(indices)
        for i in df.iloc[indices[1]:indices[2] + 1, indices[0]]:
            if i == 0:
                raise AssertionError
        self.assertEqual(x.schedule_free_at_TB(TB), False)

    # ----
    # time_block
    # ----

        # ----
        # get
        # ----

    def test_get_day_section_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_day(), "monday")
        self.assertEqual(x.get_section(), "AM")

    def test_get_hours_long_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_hours_long(), 4)

    def test_get_max_number_of_people_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_max_number_of_people(), 3)

    def test_get_number_of_people_long_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_number_of_people(), 0)

    def test_get_people_assigned_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_people_assigned(), [])

        # ----
        # checking/setting number of people
        # ----

    def test_at_max_number_of_people_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.at_max_number_of_people(), False)

    def test_add_schedules(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        y = schedule(df, "Ryan Bailey")
        z = schedule(df, "Ryan Bailey")

        x.add_schedules(y, z)
        self.assertEqual(x.get_number_of_people(), 2)
        self.assertEqual(x.get_people_assigned(), [y, z])

    def test_above_max_number_of_people_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.above_max_number_of_people(), False)

        # ----
        # get_indices
        # ----

    def test_get_indices_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        x = time_block("monday", "AM", 4, "8:00:00 AM", "12:00:00 PM", df, 3)
        self.assertEqual(x.get_indices(), [1, 2, 9])

    def test_set_priority_1(self):
        df = pd.read_csv("csv_files/Ryan_Bailey_mentor.csv", index_col=0)
        TB = time_block("tuesday", "PM", 4, "3:00:00 PM", "7:00:00 PM", df, 3)
        TB.set_priority(10)
        self.assertEqual(TB.get_priority(), 10)
    # ----
    # scheduler_assigner class
    # ----

        # ----
        # assign
        # ----
    def test_assign_for_1(self):
        def schedulize(pathname):
            name = " ".join(
                (pathname.replace('csv_files/', "")).split("_")[0:2])
            return schedule(pd.read_csv(pathname, index_col=0), name)

        import glob
        dfs = [schedulize(i) for i in glob.glob('csv_files/' + "*.csv")]
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        time_blocks = func()
        scheduler = schedule_assigner(output, dfs, time_blocks)
        scheduler.assign_for()
        out = scheduler.get_output_schedule_object()
        out.get_name_schedule()
        #.to_csv("killme.csv")

    def test_assign_while_1(self):
        def schedulize(pathname):

            # name = " ".join(
            #     (pathname.replace('csv_files\\', "")).split("_")[0:2])
            name = " ".join(
                (pathname.replace('csv_files/', "")).split("_")[0:2])
            return schedule(pd.read_csv(pathname, index_col=0), name)

        import glob
        dfs = [schedulize(i) for i in glob.glob('csv_files/' + "*.csv")]
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        time_blocks = func()
        scheduler = schedule_assigner(output, dfs, time_blocks)
        scheduler.assign_while()
        out = scheduler.get_output_schedule_object()
        # out.get_name_schedule().to_csv("killme.csv")
        # print(scheduler.status_people_under_assigned(verbose=True))
        scheduler.status_people_under_assigned(verbose=True)
        scheduler.status_people_under_assigned(verbose=False)
        scheduler.status_number_people_under_assigned()
        scheduler.status_people_num_time_blocks()
        scheduler.status_people_with_0_time_blocks()
        #print(scheduler.get_output_schedule_object().get_schedule())

        # print(scheduler.status_number_people_under_assigned())
        # print(scheduler.status_people_num_time_blocks())
        # print(scheduler.status_people_with_0_time_blocks())

    def test_assign_availability_schedule_1(self):
        def schedulize(pathname):

            # name = " ".join(
            #     (pathname.replace('csv_files\\', "")).split("_")[0:2])
            name = " ".join(
                (pathname.replace('csv_files/', "")).split("_")[0:2])
            return schedule(pd.read_csv(pathname, index_col=0), name)

        import glob
        dfs = [schedulize(i) for i in glob.glob('csv_files/' + "*.csv")]
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        time_blocks = func()
        scheduler = schedule_assigner(output, dfs, time_blocks)
        scheduler.assign_availability_schedule()
        out = scheduler.get_output_schedule_object()
        # out.get_name_schedule().to_csv("availability_killme.csv")

    def test_assign_availability_schedule_2(self):
        def schedulize(pathname):

            # name = " ".join(
            #     (pathname.replace('csv_files\\', "")).split("_")[0:2])
            name = " ".join(
                (pathname.replace('csv_files/', "")).split("_")[0:2])
            return schedule(pd.read_csv(pathname, index_col=0), name)

        import glob
        dfs = [schedulize(i) for i in glob.glob('csv_files/' + "*.csv")]
        output = pd.read_csv("Output/Output_Template.csv", index_col=0)
        output = output_schedule(output)
        time_blocks = func()
        scheduler = schedule_assigner(output, dfs, time_blocks)
        scheduler.assign_availability_schedule(True)
        out = scheduler.get_output_schedule_object()

    # ----
    # formatter
    # ----

    def test_format_to_free_for_full_hour_1(self):
        name_df = pd.read_csv("availability_schedule.csv", index_col=0)
        output_df_hour = pd.read_csv(
            "Output/Output_Template_hour.csv", index_col=0)
        form = formatter(name_df, output_df_hour)
        form.format_to_free_for_full_hour()
        form.get_name_df()
        form.get_output_df_hour()
        # form.get_output_df_hour().to_csv("killmeplz.csv")

    def test_format_to_free_for_full_hour_2(self):
        name_df = pd.read_csv("availability_schedule.csv", index_col=0)
        output_df_hour = pd.read_csv(
            "killmeplz.csv", index_col=0)
        form = formatter(name_df, output_df_hour)
        form.format_to_free_for_full_hour()
        # ----
        # main
        # ----


if __name__ == "__main__":  # pragma: no cover
    main()  # pragma: no cover
