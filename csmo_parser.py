from timer import timer

'''
time_convert_helper converts the military time
format from the .csmo to standard time format
'''


def time_convert_helper(miliTime):
    hours, minutes = miliTime.split(":")
    hours, minutes = int(hours), int(minutes)
    if len(str(minutes)) == 1:
        minutes = "0{0}".format(str(minutes))
    setting = "AM"
    if hours >= 12:
        setting = "PM"
        if hours > 12:
            hours -= 12
    return str('{0}:{1}:00 {2}'.format(hours, minutes, setting))


'''
get_index_helper accepts a list and a key as input
and returns the index of the key in the list or None
if the key is not in the list
'''


def get_index_helper(lst, key):
    i = 0
    while i < len(lst) and lst[i] != key:
        if i == len(lst) - 1:
            return None
        i += 1
    return i


'''
parse_csmo accepts a .csmo file (JSON) as input
and will look for all the time blocks in the file
it will output these to a 2D list
'''


def parse_csmo(data):
    lst = []
    for i in data['schedules'][0]['items']:
        entity = i['meetingTimes'][0]
        if len(str(entity['startMinute'])) == 1:
            stmin = '0{0}'.format(str(entity["startMinute"]))
            endmin = '0{0}'.format(str(entity["endMinute"]))
        else:
            stmin = str(entity["startMinute"])
            endmin = str(entity["endMinute"])
        lst.append([time_convert_helper('{0}:{1}'.format(entity['startHour'], stmin)), time_convert_helper('{0}:{1}'.format(entity['endHour'], endmin)),  [
            i for i in entity['days'] if entity['days'][i] is True]])
    return lst


'''
make_schedule takes the extracted .csmo and an
empty schedule as input. it returns a schedule with
time blocks filled as specified by the .csmo
'''


def make_schedule(lst, df):
    for i in lst:
        for j in i[2]:
            stidx = get_index_helper(df.index, i[0])
            endidx = get_index_helper(df.index, i[1])
            df[j][stidx:endidx] = 1
    return df


'''
parse_csmo_make_schedule takes at least one filepath a .csmo file
and the filepath to the schedule template as input. The
.csmo filename(s) must be FirstName_LastName_Student/Mentor/csmo

It reads in the .csmo(s) and parses it with parse_csmo
It creates a file name for the output file which is identical
to the input .csmo filename

It reads in an empty schedule and adds the time blocks
from the .csmo

It then saves the new schedule as a .csv.
'''


@timer
def parse_csmo_make_schedule(schedule_template_filepath, *csmo_filepaths, directory_path=None):
    import pandas as pd
    import json
    import glob
    if directory_path is None:
        for i in csmo_filepaths:
            save_filepath = i.replace('.csmo', '.csv')
            with open(i) as f:
                data = json.load(f)
            lst = parse_csmo(data)
            df = make_schedule(lst, pd.read_csv(
                schedule_template_filepath, index_col=0))
            df.to_csv(save_filepath)
    else:
        csmo_filepaths = glob.glob(directory_path + "/*.csmo")
        for i in csmo_filepaths:
            save_filepath = i.replace('.csmo', '.csv')
            with open(i) as f:
                data = json.load(f)
            lst = parse_csmo(data)
            df = make_schedule(lst, pd.read_csv(
                schedule_template_filepath, index_col=0))
            df.to_csv(save_filepath)

# Speed Test
# import time
# st = time.time()
# parse_csmo_make_schedule('Input/Input_Template.csv', 'Ryan_Bailey_mentor.csmo')
# print(time.time() - st)
