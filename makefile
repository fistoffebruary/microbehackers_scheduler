.PHONY: scheduler.log

FILES :=                              \
    algorithm.html                      \
    scheduler.log \
	algorithm.py






ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif



scheduler.html: algorithm.py
	$(PYDOC) -w algorithm

scheduler.log:
	git log > scheduler.log


# RunDiplomacy.tmp: RunDiplomacy.py RunDiplomacy1.in RunDiplomacy1.out RunDiplomacy2.in RunDiplomacy2.out RunDiplomacy3.in RunDiplomacy3.out RunDiplomacy4.in RunDiplomacy4.out RunDiplomacy5.in RunDiplomacy5.out RunDiplomacy6.in RunDiplomacy6.out RunDiplomacy7.in RunDiplomacy7.out #RunDiplomacy8.in RunDiplomacy8.out RunDiplomacy9.in RunDiplomacy9.out RunDiplomacy10.in RunDiplomacy10.out
# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy1.in >> RunDiplomacy1.tmp
# 	diff --strip-trailing-cr RunDiplomacy1.tmp RunDiplomacy1.out

# 	$(PYTHON) RunDiplomacy.py <  RunDiplomacy2.in >> RunDiplomacy2.tmp
# 	diff --strip-trailing-cr RunDiplomacy2.tmp RunDiplomacy2.out

# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy3.in >> RunDiplomacy3.tmp
# 	diff --strip-trailing-cr RunDiplomacy3.tmp RunDiplomacy3.out

# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy4.in >> RunDiplomacy4.tmp
# 	diff --strip-trailing-cr RunDiplomacy4.tmp RunDiplomacy4.out

# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy5.in >> RunDiplomacy5.tmp
# 	diff --strip-trailing-cr RunDiplomacy5.tmp RunDiplomacy5.out

# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy6.in >> RunDiplomacy6.tmp
# 	diff --strip-trailing-cr RunDiplomacy6.tmp RunDiplomacy6.out

# 	$(PYTHON) RunDiplomacy.py < RunDiplomacy7.in >> RunDiplomacy7.tmp
# 	diff --strip-trailing-cr RunDiplomacy7.tmp RunDiplomacy7.out



TestScheduler.tmp: TestScheduler.py
	$(COVERAGE) run    --branch TestScheduler.py >  TestScheduler.tmp 2>&1
	$(COVERAGE) report -m                      >> TestScheduler.tmp
	cat TestScheduler.tmp


check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  Run.tmp
	rm -f  *.tmp
	rm -rf __pycache__

config:
	git config -l

format:
	$(AUTOPEP8) -i algorithm.py
	$(AUTOPEP8) -i TestScheduler.py

scrub:
	make clean
	rm -f  scheduler.html
	rm -f  scheduler.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

versions:
	which       $(AUTOPEP8)
	$(AUTOPEP8) --version
	@echo
	which       $(COVERAGE)
	$(COVERAGE) --version
	@echo
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which       $(PYLINT)
	$(PYLINT)   --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

test: scheduler.log scheduler.html TestScheduler.tmp check
