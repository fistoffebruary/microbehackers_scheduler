# take in folder of csmo files
from csmo_parser import parse_csmo_make_schedule

from algorithm import output_schedule, schedule, schedule_assigner, time_block, formatter

from time_blocks_importer import func

import glob

import pandas as pd

from timer import timer

'''
schedulize takes a pathname to .csv and reads it into a schedule object;
converts the file name into the schedule's name
'''


def schedulize(pathname):
    # WindowsLine
    name = " ".join((pathname.replace('csmo_files\\', "")).split("_")[0:2])
    # AppleLine
    #name = " ".join((pathname.replace('csmo_files/', "")).split("_")[0:2])
    return schedule(pd.read_csv(pathname, index_col=0), name)


'''
get_output_df takes True or False as input and creates finalized
output schedules as output. Runs many methods from algorithm.py to
do this
'''


@timer
def get_output_df(parameter):

    dfs = [schedulize(i) for i in glob.glob('csmo_files/' + "*.csv")]
    output = pd.read_csv("Output/Output_Template.csv", index_col=0)
    output = output_schedule(output)
    time_blocks = func()
    scheduler = schedule_assigner(output, dfs, time_blocks)
    scheduler.assign_availability_schedule(parameter)
    output = scheduler.get_output_schedule_object().get_name_schedule()
    form = formatter(output, pd.read_csv(
        "Output/Output_Template_hour.csv", index_col=0))
    form.formatting_macro_1()
    return form.get_output_df_hour()


'''
will output two availability schedules
'''

def main():

    # create csv files from csmo files
    parse_csmo_make_schedule('Input/Input_Template.csv',
                             directory_path="csmo_files")

    # build output schedule by hour
    x = get_output_df(True)
    # write to excel file
    x.to_excel("hour_schedule.xlsx")

    # build output schedule by time block
    y = get_output_df(False)
    # write to excel file
    y.to_excel("lab_section_schedule.xlsx")


main()
