from algorithm import time_block, output_schedule
import pandas as pd


def func():
  output = pd.read_csv("Output/Output_Template.csv", index_col=0)
  mondayAM = time_block("monday", "AM", 4, "8:00:00 AM",
                        "12:00:00 PM", output, 2)
  tuesdayAM = time_block("tuesday", "AM", 4, "8:00:00 AM",
                         "12:00:00 PM", output, 2)
  wednesdayAM = time_block("wednesday", "AM", 4,
                           "8:00:00 AM", "12:00:00 PM", output, 2)
  thursdayAM = time_block("thursday", "AM", 4,
                          "8:00:00 AM", "12:00:00 PM", output, 2)
  fridayAM = time_block("friday", "AM", 4, "8:00:00 AM",
                        "12:00:00 PM", output, 2)
  saturdayAM = time_block("saturday", "AM", 4, "10:00:00 AM",
                          "2:00:00 PM", output, 2)

  mondayMID = time_block("monday", "MID", 3, "12:00:00 PM",
                         "3:00:00 PM", output, 2)
  tuesdayMID = time_block("tuesday", "MID", 3,
                          "12:00:00 PM", "3:00:00 PM", output, 2)
  wednesdayMID = time_block("wednesday", "MID", 3,
                            "12:00:00 PM", "3:00:00 PM", output, 2)
  thursdayMID = time_block("thursday", "MID", 3,
                           "12:00:00 PM", "3:00:00 PM", output, 2)
  fridayMID = time_block("friday", "MID", 3, "12:00:00 PM",
                         "3:00:00 PM", output, 2)

  mondayPM = time_block("monday", "PM", 4, "3:00:00 PM",
                        "7:00:00 PM", output, 2)
  tuesdayPM = time_block("tuesday", "PM", 4, "3:00:00 PM",
                         "7:00:00 PM", output, 2)
  wednesdayPM = time_block("wednesday", "PM", 4,
                           "3:00:00 PM", "7:00:00 PM", output, 2)
  thursdayPM = time_block("thursday", "PM", 4,
                          "3:00:00 PM", "7:00:00 PM", output, 2)
  fridayPM = time_block("friday", "PM", 4, "3:00:00 PM",
                        "7:00:00 PM", output, 2)
  return [mondayAM, mondayMID, mondayPM, tuesdayAM, tuesdayMID, tuesdayPM, wednesdayAM, wednesdayMID, wednesdayPM, thursdayAM, thursdayMID, thursdayPM, fridayAM, fridayMID, fridayPM, saturdayAM]
